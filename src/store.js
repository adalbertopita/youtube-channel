import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from "redux-thunk";
import logger from 'redux-logger';
import promise from 'redux-promise-middleware';

import videosReducer from './app/reducers/videos';
import videoViewReducer from './app/reducers/videoView';

export default createStore(
    combineReducers({
        videosReducer,
        videoViewReducer
    }),
      applyMiddleware(
        promise(),
        thunk,
        logger()
      )
);
