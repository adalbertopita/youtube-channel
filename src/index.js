import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, IndexRoute } from "react-router";
import {Provider} from "react-redux";

const css = require('./styles/main.scss');

import Layout from "./app/components/layout";

import Videos from './app/containers/videos';
import VideoView from './app/containers/videoView';

import store from './store';

class App extends Component {
  render() {
    return (
      <Router history={browserHistory}>
          <Route path={"/"} component={Layout} >
              <IndexRoute component={Videos} />
              <Route path='videos' component={Videos} />
              <Route path='video/:id' component={VideoView} />
          </Route>
      </Router>
    );
  }
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
