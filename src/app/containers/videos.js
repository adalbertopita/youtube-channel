import { connect } from 'react-redux';

import Videos from '../components/videos';
import {fetchVideos} from '../actions/videos';

const mapStateToProps = (state) => ({
  data: state.videosReducer,
  pageToken: state.videosReducer.pageToken,
  query: state.videosReducer.query
})

const mapDispatchToProps = (dispatch) => {
  return {
    fetchVideos: (pageToken, query) => {
      dispatch(fetchVideos(pageToken, query))
    }
  }
}

const VideosContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Videos)

export default VideosContainer;
