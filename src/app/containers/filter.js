import { connect } from 'react-redux';

import Filter from '../components/filter';
import {fetchVideos} from '../actions/videos';

const mapStateToProps = (state) => ({
  data: state.videosReducer,
  pageToken: state.videosReducer.pageToken,
  query: state.videosReducer.query
})

const mapDispatchToProps = (dispatch) => {
  return {
    fetchVideos: (pageToken, query) => {
      dispatch(fetchVideos(pageToken, query))
    }
  }
}

const FilterContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Filter)

export default FilterContainer;
