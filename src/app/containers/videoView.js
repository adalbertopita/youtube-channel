import { connect } from 'react-redux';

import VideoView from '../components/videoView';
import {fetchVideoView} from '../actions/videos';

const mapStateToProps = (state) => ({
  data: state.videoViewReducer
})

const mapDispatchToProps = (dispatch) => {
  return {
    fetchVideoView: (id) => {
      dispatch(fetchVideoView(id))
    }
  }
}

const VideoViewContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(VideoView)

export default VideoViewContainer;
