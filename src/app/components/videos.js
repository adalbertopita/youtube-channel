import React from 'react';
import Button from './button';
import VideoItem from './videoItem';

import Filter from "./filter";

class Videos extends React.Component {
  constructor(props) {
    super(props);
    this.fetchData = this.fetchData.bind(this);
  }

  componentDidMount(){
    this.fetchData();
  }

  fetchData(){
    this.props.fetchVideos(this.props.pageToken, this.props.query);
  }

  render() {
    const { data } = this.props;
    return (
      <div className="row">
        <hr/>
          {data.videos.map((vid, x) => <VideoItem
                        img={vid.thumbnails.medium.url}
                        key={x}
                        id={vid.id}
                        title={vid.title}
                        onClick={() => setVideo(vid)}
                    />
            )}
        <a onClick={this.fetchData} className="btn btn-primary">view more</a>
      </div>
    )
  }
}

export default Videos;
