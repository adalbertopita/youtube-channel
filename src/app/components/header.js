import React, { Component } from 'react';
import Filter from "../containers/filter";

class Header extends Component {
  render() {
    return (
      <div>
          <nav className="navbar navbar-inverse navbar-fixed-top">
            <div className="container">
              <div className="navbar-header">
                <a className="navbar-brand" href="#">Youtube</a>
              </div>
              <Filter/>
            </div>
          </nav>
      </div>
    );
  }
}

export default Header;
