import React from 'react';
import { Link } from 'react-router';

const VideoItem = (props) => (
  <div className="video-item col-md-4">
      <Link to={`/video/${props.id}`}>
        <img width="310" height="180" src={props.img} alt="" />
      </Link>
      <h5>{props.title}</h5>
  </div>
)


export default VideoItem
