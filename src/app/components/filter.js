import React from 'react';

class Filter extends React.Component {
  constructor(props) {
    super(props);
  }

  handleSubmit(e){
    e.preventDefault();
    var el = document.querySelector(".search-text");
    this.props.fetchVideos("",el.value);
  }

  render() {
    return (
      <div className="row">
        <form className="navbar-form navbar-right" onSubmit={this.handleSubmit.bind(this)}>
          <div className="form-group">
            <input type="text" placeholder="search" className="search-text form-control" />
            <button type="submit" className="btn btn-success">search</button>
          </div>
        </form>
      </div>
    )
  }
}

export default Filter;
