// import React from 'react';
//
// const videoView = (props) => {
//   let video_url = "https://www.youtube.com/embed/" + props.params.id.replace(':','');
//   return (
//       <div className="row">
//           <iframe width="854" height="480" src={video_url} frameBorder="0" allowFullScreen></iframe>
//       </div>
//   );
// }
//
// export default videoView;


import React from 'react';

class videoView extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
    this.props.fetchVideoView(this.props.params.id.replace(':',''));
  }

  render() {
    const { videoView } = this.props.data;
    let video_url = "https://www.youtube.com/embed/" + videoView.id;
    return(
      <div>
        <h1>{videoView.title}</h1>
        <iframe width="854" height="480" src={video_url} frameBorder="0" allowFullScreen></iframe>
        <p>{videoView.description}</p>
        <p>{videoView.viewCount} views</p>
      </div>
    )
  }
}

export default videoView;
