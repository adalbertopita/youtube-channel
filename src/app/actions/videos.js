import axios from 'axios';

const ENDPOINT = "https://www.googleapis.com/youtube/v3",
      CHANNEL = "UCd-P95WFOH6aK3z3apWgISQ",
      API_KEY = "AIzaSyDDnmuHe-9isdaaaIZ7mMHdp6q6llPddpY";

export function fetchVideos(pageToken, query) {
    return function(dispatch){

      var url = `${ENDPOINT}/search?&key=${API_KEY}&channelId=${CHANNEL}&part=snippet,id&order=date&maxResults=20`;

      if(pageToken){
        url = url + "&pageToken="+pageToken;
      }

      if(query){
        url = url + "&q="+query;
      }

      console.log(url);

        axios.get(url)
        .then((response)=>{

          var arrVideos = [],
              items = response.data.items;

            items.map((vid, x) => {
              arrVideos.push({
                id : vid.id.videoId,
                title: vid.snippet.title,
                description: vid.snippet.description,
                thumbnails: vid.snippet.thumbnails,
                publishedAt: vid.snippet.publishedAt
              });
            });

            dispatch({
                type: "FETCH_VIDEOS",
                payload: arrVideos
            });

            dispatch({
                type: "UPDATE_PAGETOKEN",
                payload: response.data.nextPageToken
            });
        })
        .catch((err)=> {
            console.log('erro: ', err)
        })
    };
}

export function fetchVideoView(id){
  return function(dispatch){

    var url = `${ENDPOINT}/videos?&key=${API_KEY}&channelId=${CHANNEL}&part=snippet,id,statistics&order=date&maxResults=20&id=${id}`;

    axios.get(url)
    .then((response)=>{
      let vid = response.data.items[0];
      dispatch({
          type: "GET_VIDEO",
          payload: {
            title: vid.snippet.title,
            thumbnails: vid.snippet.thumbnails,
            description: vid.snippet.description,
            publishedAt: vid.snippet.publishedAt,
            id: vid.id,
            viewCount: vid.statistics.viewCount
          }
      });
    })



  }
}

export function setQuery(query) {
  return{
    type: 'SET_QUERY',
    payload: query
  }
}
