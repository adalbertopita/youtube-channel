const videosReducer = (state = {
    videos: [],
    pageToken: "",
    query: ""
}, action) => {
    switch (action.type) {
        case "FETCH_VIDEOS" :
          state = {...state, videos: state.videos.concat(action.payload) };
          break;

        case "SET_QUERY" :
          state = {...state, query: action.payload };
          break;

        case "UPDATE_PAGETOKEN" :
          state = {...state, pageToken : action.payload }
        default:
            return state
    }
    return state;
};

export default videosReducer;
