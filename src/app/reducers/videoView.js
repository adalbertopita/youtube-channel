const videoViewReducer = (state = {
    videoView: []
}, action) => {
    switch (action.type) {
        case "GET_VIDEO" :
          state = {...state, videoView: action.payload };
          break;
        default:
            return state
    }
    return state;
};

export default videoViewReducer;
